const event = document.getElementById('event');

const isFirefox = (/Firefox/i.test(navigator.userAgent));
const isIe = (/MSIE/i.test(navigator.userAgent)) || (/Trident.*rv:11\./i.test(navigator.userAgent));

const scrollSensitivitySetting = 30; // 滑动灵敏度（越大越不敏感）
const slideDurationSetting = 600; // 滑片锁定时间
const totalSlideNumber = document.getElementsByClassName("background").length;
let currentSlideNumber = 0;
let ticking = false;

function parallaxScroll(evt) {
  let delta;
  if (isFirefox) {
    delta = evt.detail * (-120);
  } else if (isIe) {
    delta = -evt.deltaY;
  } else {
    delta = evt.wheelDelta;
  }

  if (!ticking) {
    if (delta <= -scrollSensitivitySetting) {
      // 向下滚动
      nextPage();
      emitPageChangeEvent(+1);
    }
    if (delta >= scrollSensitivitySetting) {
      // 向上滚动
      prevPage();
      emitPageChangeEvent(-1);
    }
  }
}

function slideDurationTimeout(slideDuration) {
  setTimeout(function () {
    ticking = false;
  }, slideDuration);
}

const mousewheelEvent = isFirefox ? "DOMMouseScroll" : "wheel";
window.addEventListener(mousewheelEvent, _.throttle(parallaxScroll, 60), false);

function nextPage() {
  ticking = true;
  if (currentSlideNumber !== totalSlideNumber - 1) {
    currentSlideNumber++;
    const previousSlide = eq.call(document.getElementsByClassName("background"), currentSlideNumber - 1);
    previousSlide.classList.remove("up-scroll");
    previousSlide.classList.add("down-scroll");
  }
  slideDurationTimeout(slideDurationSetting);
}

function prevPage() {
  ticking = true;
  if (currentSlideNumber !== 0) {
    currentSlideNumber--;
  }
  const currentSlide = eq.call(document.getElementsByClassName("background"), currentSlideNumber);
  currentSlide.classList.remove("down-scroll");
  currentSlide.classList.add("up-scroll");
  slideDurationTimeout(slideDurationSetting);
}

function jumpPage(to) {
  let delta = to - current_page;
  if (delta > 0) {
    for (let i = 0; i < delta; i++) {
      nextPage();
    }
  } else if (delta < 0) {
    for (let i = 0; i > delta; i--) {
      prevPage();
    }
  }
  emitPageChangeEvent(delta);
}

function eq(index) {
  if (index >= 0 && index < this.length) {
    return this[index];
  } else {
    return -1;
  }
}

function emitPageChangeEvent(delta) {
  event.dispatchEvent(new CustomEvent('pagechange', {
    detail: {
      delta,
    }
  }));
}