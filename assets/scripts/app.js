/**
 * 简易模板引擎
 */
class Template {
  /**
   * 构造模板
   * @param id {string} 模板ID
   */
  constructor(id) {
    this.id = id;
  }

  /**
   * 获取模板内容
   * @returns {Template}
   */
  fetchTemplate() {
    const template = document.getElementById(this.id);
    this.template = template.innerHTML;
    return this;
  }

  /**
   * 注入变量
   * @param variables {{name: value}} 变量
   * @returns {Template}
   */
  injectVariables(variables) {
    for (name in variables) {
      this.template = this.template.replaceAll(`{{${name}}}`, variables[name]);
    }
    return this;
  }

  /**
   * 渲染模板
   * @returns {string}
   */
  renderTemplate() {
    return this.template;
  }

  /**
   * 追加至元素末尾
   * @param node {Element} 追加至的元素
   */
  appendTo(node) {
    node.insertAdjacentHTML('beforeend', this.renderTemplate());
  }
}

/**
 * 导航栏索引
 * @type {string[]}
 */
let menu = [];

/**
 * 当前页数
 * @type {number}
 */
let current_page = 0;

/**
 * 构建导航栏
 */
(function () {
  // 获取导航栏模板
  const menu_template = document.querySelector('template#template-menu');
  console.log(menu_template);

  menu_template.content.querySelectorAll('a[href="javascript:"]').forEach((item, index) => {
    // 构建导航栏索引，页数为键，标题为值
    menu[index] = item.innerText;
    item.setAttribute('data-page-num', index.toString());
  });

  // 获取所有页面
  const content_wrappers = document.querySelectorAll('.content-wrapper');

  // 遍历页面，插入导航栏
  console.group('MENU BUILDING');
  console.debug('total of', content_wrappers.length, 'pages');
  content_wrappers.forEach((content_wrapper, page_num) => {
    console.debug(`building page`, page_num, menu[page_num]);

    // 判断当前页面是否为中间页面
    let in_middle = true;
    if (page_num <= 0 || page_num >= content_wrappers.length - 1) {
      in_middle = false;
    }

    // 复制导航栏模板
    const page_menu = menu_template.content.cloneNode(true);
    // 获取所有导航项
    const menu_items = page_menu.querySelectorAll('a');
    // 为当前页面的导航项加上样式
    menu_items[page_num].classList.add('active');
    // 插入到内容中
    content_wrapper.append(page_menu);

    // 添加 class 方便特定于中间页面的样式处理
    if (in_middle) {
      content_wrapper.classList.add('in-middle');
    }

    console.debug('  --> DONE');
  });
  console.groupEnd();
})();

/**
 * 解析 URL Hash
 *
 * @returns {{}}
 */
const parse_hash = () => {
  // 将 hash 按 & 切割为一组，再用 = 分隔，从而将 hash 转为对象
  return location.hash.substring(1).split('&').reduce((res, item) => {
    const parts = item.split('=');
    res[parts[0]] = parts[1];
    return res;
  }, {})
};

/**
 * 设置 URL Hash
 *
 * @param hash
 */
const set_hash = hash => {
  location.hash = Object.keys(hash).map(k => k + '=' + hash[k]).join('&');
}

/**
 * 确保传入的数字没有超出范围
 * @param number {number}
 * @param min {number}
 * @param max {number}
 * @returns {number}
 */
const ensure_range = (number, min, max) => {
  if (number < min) {
    number = min;
  } else if (number > max) {
    number = max;
  }
  return number;
}

/**
 * 监听页面变更事件并更新 Hash
 */
event.addEventListener('pagechange', e => {
  const delta = e.detail.delta;
  current_page += delta;
  current_page = ensure_range(current_page, 0, menu.length - 1);
  set_hash({
    page: current_page,
    title: menu[current_page],
  });

  document.querySelector('.container').dataset.page = current_page.toString();

  console.debug('page changed', current_page - delta, '-->', current_page);
});

/**
 * 监听 DOM 加载完成事件，跳转至目标页面
 */
document.addEventListener('DOMContentLoaded', () => {
  const { page, title } = parse_hash();
  if (encodeURI(menu[page]) === title) {
    jumpPage(page);
  } else {
    console.warn('Page not synced.');
  }
});

// 导航栏点击事件，因为是动态添加的，因此需要动态监听
document.querySelector('body').addEventListener('click', e => {
  // 目标为 a 元素且存在 data-page-num，即为导航链接
  if (e.target.tagName.toLowerCase() === 'a' && Number(e.target.dataset.pageNum) >= 0) {
    jumpPage(Number(e.target.dataset.pageNum));
  }
});

// 构建轮播图
(function () {
  const carousel_item_template = new Template('template-carousel-item');

  console.group('CAROUSEL BUILDING');
  document.querySelectorAll('.carousel').forEach((carousel, index) => {
    const id = `carousel-${index}`;
    const inner = carousel.querySelector('.carousel-inner');
    const data = carousel.querySelector('datalist').querySelectorAll('data');
    let items = [];

    console.group(id);

    const log = (...data) => {
      console.debug(...data);
    }

    log('building', id);

    // 将 datalist 元素内容解析为数据数组
    data.forEach(datum => {
      const value = datum.value.split('#(');
      items.push({
        text: value[0],
        image: datum.innerText,
        description: value[1],
        title: datum.dataset.title,
        subtitle: datum.dataset.subtitle,
		link: datum.dataset.link || '#'
      });
    });
    log('items prepared', items);

    // 渲染轮播图页面
    items.forEach((item, index) => {
      carousel_item_template.fetchTemplate()
        .injectVariables({
          id,
          index,
          ...item,
        }).appendTo(inner);

      log('slide inserted', `${id}-slide-${index}`);
    });

    // 渲染轮播图页面控制项
    items.forEach((item, index) => {
      const label = document.createElement('label');
      label.classList.add('carousel-control', `control-${id}-slide-${index}`);

      const prev = label.cloneNode(true);
      prev.classList.add('prev');
      let prev_index = index - 1;
      if (prev_index < 0) {
        prev_index = items.length - 1;
      }
      prev.setAttribute('for', `${id}-slide-${prev_index}`);
      prev.innerText = '‹';

      const next = label.cloneNode(true);
      next.classList.add('next');
      let next_index = index + 1;
      if (next_index > items.length - 1) {
        next_index = 0;
      }
      next.setAttribute('for', `${id}-slide-${next_index}`);
      next.innerText = '›';

      inner.append(prev);
      inner.append(next);

      log('control inserted', `control-${id}-slide-${index}`);
    });

    // 渲染轮播图指示器（小点点）
    const indicators = document.createElement('ol');
    indicators.classList.add('carousel-indicators');
    items.forEach((item, index) => {
      const li = document.createElement('li');
      li.innerHTML = `<label class="carousel-bullet" for="${id}-slide-${index}">•</label>`;
      indicators.append(li);

      log('indicators inserted', `${id}-slide-${index}`);
    });
    inner.append(indicators);
	document.getElementById(`${id}-slide-0`).setAttribute('checked', true);
    console.groupEnd();
  });
  console.groupEnd();
})();

// 构建时间轴
(function () {
  const timeline_item_template = new Template('template-timeline-item');

  document.querySelectorAll('.timeline').forEach((timeline, index) => {
    const data = timeline.querySelector('datalist').querySelectorAll('data');
    let items = [];

    // 将 datalist 元素内容解析为数据数组
    data.forEach(datum => {
      items.push({
        time: datum.value,
        title: datum.dataset.title,
        content: datum.innerHTML,
		image: datum.dataset.image,
      });
    });

    // 将数据进行分片，每组五个
    const pages = _.chunk(items, 4);

    // 当前页面
    let tl_page = 0;

    // 根据页数渲染时间轴元素
    const render_timeline_items = page => {
      // 先清除所有元素
      while (timeline.firstChild) {
        timeline.removeChild(timeline.firstChild);
      }
      // 然后插入对应页面的元素
      pages[page].forEach(item => {
        timeline_item_template.fetchTemplate()
          .injectVariables(item)
          .appendTo(timeline);
      });
    }
    // 初始为第一页
    render_timeline_items(tl_page);

    // 监听上一页
    const prev_btn = timeline.parentNode.querySelector('.prev');
    prev_btn.addEventListener('click', () => {
      tl_page = ensure_range(--tl_page, 0, pages.length - 1);
      render_timeline_items(tl_page);
    });

    // 监听下一页
    const next_btn = timeline.parentNode.querySelector('.next');
    next_btn.addEventListener('click', () => {
      tl_page = ensure_range(++tl_page, 0, pages.length - 1);
      render_timeline_items(tl_page);
    });
  })
})();